package com.example.todo_list;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private DatabaseHelper dbHelper;

    private ArrayList<String> tasks;
    private ArrayAdapter<String> taskAdapter;
    private ListView tasksListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbHelper = new DatabaseHelper(this);

        tasksListView = (ListView) findViewById(R.id.lvItems);
        tasks = new ArrayList<String>();
        taskAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, tasks);
        tasksListView.setAdapter(taskAdapter);
        loadTaskList();
        setupListViewListener();
    }

    public void addTask(View v) {
        EditText etNewItem = (EditText) findViewById(R.id.etNewItem);
        String itemText = etNewItem.getText().toString();
        dbHelper.addTask(itemText);
        loadTaskList();
        //itemsAdapter.add(itemText);
        etNewItem.setText("");
    }

    private void loadTaskList() {
        ArrayList<String> taskList = dbHelper.getTaskList();
        if(taskAdapter == null){
            taskAdapter = new ArrayAdapter<String>(this,R.layout.row,R.id.task_title,taskList);
            tasksListView.setAdapter(taskAdapter);
        }
        else{
            taskAdapter.clear();
            taskAdapter.addAll(taskList);
            taskAdapter.notifyDataSetChanged();
        }
    }
    private void setupListViewListener() {
        tasksListView.setOnItemLongClickListener(
                new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> adapter,
                                                   View item, int pos, long id) {
                        //remove from db
                        String textToDelete = tasks.get(pos).split("\n", 2)[0];
                        dbHelper.removeTask(textToDelete);
                        loadTaskList();
                        return true;
                    }

                });
        tasksListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), EditActivity.class);
                intent.putExtra("message_key", tasks.get(position).toString());
                startActivity(intent);
            }
        });
    }

}