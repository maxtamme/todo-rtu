package com.example.todo_list;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class EditActivity extends AppCompatActivity {

    Button saveBtn;
    EditText inputField;
    String oldValue;

    DatabaseHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        dbHelper = new DatabaseHelper(this);

        saveBtn = (Button) findViewById(R.id.button);
        inputField = (EditText) findViewById(R.id.plain_text_input);

        Intent intent = getIntent();
        oldValue = intent.getStringExtra("message_key").split("Created at:")[0].replaceAll("\\s+$", "");
        inputField.setText(oldValue);

        saveBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                dbHelper.updateTask(inputField.getText().toString(), oldValue);
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });
    }
}