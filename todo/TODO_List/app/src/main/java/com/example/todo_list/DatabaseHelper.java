package com.example.todo_list;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static int db_version = 1;
    private static String db_name = "todo_db";
    private static String table_name = "todo";
    private static String id = "ID";
    private static String action = "task";
    private static String dateColumn = "creation_date";

    public DatabaseHelper(Context context) {
        super(context, db_name, null, db_version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = String.format("CREATE TABLE %s (ID INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT NOT NULL, %s TEXT NOT NULL);", table_name, action, dateColumn);
        db.execSQL(query);
    }

    public String getDate() {
        Calendar calendar = Calendar.getInstance(); // Returns instance with current date and time set
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        return formatter.format(calendar.getTime());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String query = String.format("DROP TABLE IF EXISTS %s", db_name);
        db.execSQL(query);
        onCreate(db);
    }

    public void addTask(String task) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(action, task);
        contentValues.put(dateColumn, getDate());
        db.insert(table_name, null, contentValues);
        db.close();
    }

    public void removeTask(String task) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(table_name, action+"=?", new String[] {task});
        db.close();
    }

    public void updateTask(String newTask, String oldTask) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(action, newTask);
        contentValues.put(dateColumn, getDate());
        db.update(table_name, contentValues, action+"=?", new String[] {oldTask});
    }

    public ArrayList<String> getTaskList(){
        ArrayList<String> taskList = new ArrayList<>();
        ArrayList<String> dateList = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(table_name,new String[]{action, dateColumn},null,null,null,null,null);
        while(cursor.moveToNext()){
            int indexTask = cursor.getColumnIndex(action);
            int indexDate = cursor.getColumnIndex(dateColumn);
            taskList.add(cursor.getString(indexTask) + "\n Created at: " + cursor.getString(indexDate));
        }
        /*cursor.close();
        cursor = db.query(table_name,new String[]{dateColumn},null,null,null,null,null);
        while(cursor.moveToNext()){
            int index = cursor.getColumnIndex(dateColumn);
            taskList.set(index, taskList.get(index) + "     "  + cursor.getString(index));
        }*/
        cursor.close();
        db.close();
        return taskList;
    }
}
